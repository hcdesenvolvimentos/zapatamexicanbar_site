<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Zapata_Mexican_Bar
 */
global $configuracao;
?>




	<div class="modalGeral fazerReserva">
		<div class="formularioReserva formReserva">
			<i class="fa closeModal fa-times" aria-hidden="true"></i>
			<p>Solicitar Reserva</p>
			<?php 
				 echo do_shortcode($configuracao['areaReservas_formulario_tagme']);

			 ?>
		</div>
	</div>

	<div class="modalGeral modal360">
		<div class="formularioReserva ver360">
			<i class="fa closeModal fa-times" aria-hidden="true"></i>
			<iframe src="https://www.google.com/maps/embed?pb=!1m0!4v1504144318428!6m8!1m7!1sCAoSLEFGMVFpcE5vR2M2VEFSOTQ4S1htRVhmMm0wRFlLTmhScVR3RkNpMUtOd2dW!2m2!1d-25.40731889166361!2d-49.27067846390179!3f58.19912907519235!4f0!5f0.7820865974627469" class="iframe360" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>

	<!-- RODAPÉ -->
	<footer class="rodape">

		<div id="TA_certificateOfExcellence713" class="TA_certificateOfExcellence">
			<ul id="zX2ksnC" class="TA_links eGjzHzcb">
				<li id="Gehsjkw" class="CeSQnRK">
					<a target="_blank" href="https://www.tripadvisor.com.br/Restaurant_Review-g303441-d911408-Reviews-Zapata_Mexican_Bar_Centro_Civico-Curitiba_State_of_Parana.html"><img class="hvr-push" src="https://www.tripadvisor.com.br/img/cdsi/img2/awards/CoE2017_WidgetAsset-14348-2.png" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/></a>
				</li>
			</ul>
		</div>
		<script src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=713&amp;locationId=911408&amp;lang=pt&amp;year=2017&amp;display_version=2"></script>

		<div id="TA_excellent378" class="TA_excellent">
			<ul id="0eRy7ETFIY" class="TA_links SFgpW0P0IN">
				<li id="7IrvSvTY" class="7gXzf2DPgWW">
					<a target="_blank" href="https://www.tripadvisor.com.br/Restaurant_Review-g303441-d911408-Reviews-Zapata_Mexican_Bar_Centro_Civico-Curitiba_State_of_Parana.html"><img class="hvr-push"  src="https://static.tacdn.com/img2/widget/tripadvisor_logo_115x18.gif" alt="TripAdvisor" class="widEXCIMG" id="CDSWIDEXCLOGO"/></a>
				</li>
			</ul>
		</div>
		<script src="https://www.jscache.com/wejs?wtype=excellent&amp;uniq=378&amp;locationId=911408&amp;lang=pt&amp;display_version=2"></script>
		<!-- MAPA -->
		<div class="mapa">
			<div class="mapa360">
				<div class="areabtn">
					<button id="btn360">360° <i class="fa fa-eye" aria-hidden="true"></i></button>
					<a href="https://www.google.com.br/maps/place/R.José Sabóia Cortês, 383 - Centro Cívico, Curitiba - PR, 80530-000" target="_blank">Ver endereço <i class="fa fa-map-marker" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="" id="map"></div>
		</div>
		
		<!-- ÁREA INFORMAÇÕES RODAÉ -->
		<div class="infoRodape">
			<div class="containerFooter">
				

				<div class="redesSociais">
					<a href="<?php echo $configuracao['contato_facebook'] ?>" target="_blank">
						<i class="fa fa-facebook-official" aria-hidden="true"></i>
					</a>
					<a href="<?php echo $configuracao['contato_instagram'] ?>" target="_blank">
						<i class="fa fa-instagram" aria-hidden="true"></i>
					</a>
					<a href="<?php echo $configuracao['contato_twitter'] ?>" target="_blank">
						<i class="fa fa-twitter" aria-hidden="true"></i>
					</a>
				</div>

					<strong>Mapa do site</strong>
					<?php 
						$menu = array(
							'theme_location'  => '',
							'menu'            => 'Menu Principal',
							'container'       => false,
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => '',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 2,
							'walker'          => ''
							);
						wp_nav_menu( $menu );
					?>
			</div>
		</div>
		<div class="copyright">
			<div class="containerFooter">
				<img src="<?php echo $configuracao['rodape_logo']['url'] ?>" alt="Zapata Mexican Bar">
				<p><i class="fa fa-copyright" aria-hidden="true"></i> <?php echo $configuracao['rodape_copyright'] ?></p>
			</div>
		</div>
	</footer>
<?php wp_footer(); ?>

</body>
</html>

	

<?php 
	$endereco1 = $configuracao['contato_rua'].$configuracao['contato_cidade'].$configuracao['contato_cep'];
	// FUNÇÃO MAPA 
	// $geo   = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($endereco1).'&sensor=false');
	// $geo   = json_decode($geo, true);

	
		$latitude = -25.4072734;
		$longitude = -49.2705904;

 ?>

 	<!-- FUNÇÃO PARA ADICIONAR ICONE NO MAPA -->
	<script>

		var map;
		//POSICIONAMENTO DOS ESTABALECIMENTOS
		var endereco = {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>};

		

		var enderecoMapa1 = "https://www.google.com.br/maps/place/R.José Sabóia Cortês, 383 - Centro Cívico, Curitiba - PR, 80530-000";
		var enderecoMapa = "https://www.google.com.br/maps/place/R.José Sabóia Cortês, 383 - Centro Cívico, Curitiba - PR, 80530-000";


		//ICONES DOS ESTABELECIMENTOS
		var icone1 = '<?= get_bloginfo("template_url"); ?>' + '/img/pin.png';
	
		
		function initMap() {

			//MAPA
			map = new google.maps.Map(document.getElementById('map'), {
				center: endereco,
				zoom: 18,
				zoomControl: true,
				scrollwheel: false,
				draggable: false, 
				disableDoubleClickZoom: true,
				
			});

			//MARKERS OU PINS DO MAPA
			var enderecoMap = new google.maps.Marker({
			    position: endereco,
			    icon: icone1,
			    map: map
			});

			google.maps.event.addListener(enderecoMap, 'click', function(e) {
				window.open(enderecoMapa);
				
			});

			//MAPA
			mapContato = new google.maps.Map(document.getElementById('mapContato'), {
				center: endereco,
				zoom: 18,
				zoomControl: true,
				scrollwheel: false,
				draggable: false, 
				disableDoubleClickZoom: true,
				
			});

			//MARKERS OU PINS DO MAPA
			var enderecoMap1 = new google.maps.Marker({
			    position: endereco,
			    icon: icone1,
			    map: mapContato
			});

			google.maps.event.addListener(enderecoMap1, 'click', function(e) {
				window.open(enderecoMapa1);
				
			});

		}
	
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&callback=initMap" async defer></script>