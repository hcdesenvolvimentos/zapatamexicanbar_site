

<?php
/**
 * Template Name: Inicial
 * Description: 
 *
 * @package Zapata_Mexican_Bar
 */
global $configuracao;

get_header(); ?>
	<!-- PG INICIAL -->
	<div class="pg pg-inicial">
		
		<!-- CARROSSEL DE DESTAQUE -->
		<section class="carrosselDestaque sessao">
			<h3 class="hidden">Destaques</h3>
			<?php 
				//LOOP DE POST CATEGORIA DESTAQUE				
				$destaques = new WP_Query(array(
					'post_type'     => 'destaques',
					'posts_per_page'   => -1,
					'tax_query'     => array(
						array(
							'taxonomy' => 'categoriaDestaque',
							'field'    => 'slug',
							'terms'    => 'banner-principal',
							)
						)
					)
				);

				if ($destaques):
			?>
			<div id="carrosselDestaque" class="owl-Carousel">
				<?php 
					
					// LOOP DE POST
					while ( $destaques->have_posts() ) : $destaques->the_post();
						//FOTO DESTACADA
						$fotoDestque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoDestque = $fotoDestque[0];
						//IMAGEM MOBILIE
						$destaque_mobile = rwmb_meta('baseZapatamexicanBar_destaque_mobile');
						// LINK DESTAQUE
						
						$destaque_link = rwmb_meta('baseZapatamexicanBar_destaque_link');
						foreach ($destaque_mobile  as $destaque_mobile):
							$destaque_mobile = $destaque_mobile['full_url']; 
					?>		
				<!-- ITEM -->
				<figure class="item" style="background: url(<?php echo $destaque_mobile ?>);">
				<a href="<?php echo $destaque_link  ?>" target="_blank">
					<img class="img-responsive" src="<?php echo $fotoDestque ?>" alt="<?php echo get_the_title() ?> ">
				</a>
				</figure>
				<?php  endforeach; endwhile; wp_reset_query();  ?>
		
			</div>
			<?php endif; ?>
		</section>

		<!-- CARROSSEL DE PORMOÇÕES -->
		<div class="container">
			<section class="carrosselPromocao sessao">
				<h3 class="hidden">Destaques Promoções</h3>

				<!-- TÍTULO -->
				<p class="tiutoModeloRight">Destaques:</p>
				<?php 
					//LOOP DE POST CATEGORIA DESTAQUE				
					$destaques = new WP_Query(array(
						'post_type'     => 'destaques',
						'posts_per_page'   => -1,
						'tax_query'     => array(
							array(
								'taxonomy' => 'categoriaDestaque',
								'field'    => 'slug',
								'terms'    => 'banner-secundario',
								)
							)
						)
					);
					if ($destaques):
				?>
				<div id="carrosselPromocao" class="owl-Carousel">
					<?php 
						
						// LOOP DE POST
						while ( $destaques->have_posts() ) : $destaques->the_post();
							//FOTO DESTACADA
							$fotoDestque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoDestque = $fotoDestque[0];
							//IMAGEM MOBILIE
							$destaque_mobile = rwmb_meta('baseZapatamexicanBar_destaque_mobile');
							// LINK DESTAQUE
							
							$destaque_link = rwmb_meta('baseZapatamexicanBar_destaque_link');
							foreach ($destaque_mobile  as $destaque_mobile):
								$destaque_mobile = $destaque_mobile['full_url']; 
						?>		
					<!-- ITEM -->
					<figure class="item" style="background: url(<?php echo $destaque_mobile ?>);">
					<a href="<?php echo $destaque_link  ?>" target="_blank">
						<img class="img-responsive" src="<?php echo $fotoDestque ?>" alt="Nome destaque">
					</a>
					</figure>
					<?php  endforeach; endwhile; wp_reset_query();  ?>

				</div>
				<?php endif; ?>
			</section>
		</div>

		<!-- SESSÃO RESERVAS -->
		<section class="sessaoReservas sessao">
			<h3 class="hidden">Reservas</h3>
			<!-- TÍTULO -->
			<p class="tiutoModeloRight left">Reservas</p>
			
			<div class="row">
				
				<!-- FOTO ILUSTRATIVA -->
				<div class="col-sm-4  col-xs-2">
					<figure class="imgIlustrativa left">
						<img class="img-responsive" src="<?php echo $configuracao['areaReservas_imagemLeft']['url'] ?>" alt="Reserva">
					</figure>
				</div>

				<div class="col-sm-4  col-xs-8">
					<address class="areaReservas">
						<h3><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $configuracao['contato_nome'] ?></h3>
						<p><?php echo $configuracao['contato_rua'] ?></p>
						
						<button class="btnReserva">Fazer reserva</button>
						<button class="btnReservaRestorando" >Reservar com Restorando</button>
						<br>	
						<p><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['contato_duvidas'] ?></p>

					</address>
				</div>
				
				<!-- FOTO ILUSTRATIVA -->
				<div class="col-sm-4 col-xs-2">
					<figure class="imgIlustrativa right">
						<img class="img-responsive" src="<?php echo $configuracao['areaReservas_imagemRight']['url'] ?>" alt="Reserva">
					</figure>
				</div>

			</div>
		</section>

	</div>
<?php get_footer(); ?>
