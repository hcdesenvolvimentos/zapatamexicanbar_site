

<?php
/**
 * Template Name: A Marca
 * Description: 
 *
 * @package Zapata_Mexican_Bar
 */
global $configuracao;

get_header(); ?>
	<!-- PG A MARCA -->
	<div class="pg pg-marca">
		
		<!-- BANNER TOPO -->
		<figure  class="bannerTopo" style="background:url(<?php echo $configuracao['marca_banner']['url'] ?>)"></figure>
		<small  id="a-marca"></small>
		<!-- SOBRE NÓS  -->
		<section class="aMarca">
			
			<!-- TÍTULO -->
			<div class="areaTitulos">
				<h4 class="tituloInternos"><?php echo get_the_title() ?></h4>
			</div>

			<!-- TEXTO -->
			<div class="container">
				<article>
					<?php echo the_content() ?>
					
					<figure class="areaImagem">
						<img src="<?php echo $configuracao['marca_img']['url'] ?>" alt="<?php echo get_the_title() ?>">
					</figure>
				</article>
			</div>

		</section>

	</div>
<?php get_footer(); ?>
