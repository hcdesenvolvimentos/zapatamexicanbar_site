

<?php
/**
 * Template Name: Contato
 * Description: 
 *
 * @package Zapata_Mexican_Bar
 */
global $configuracao;

get_header(); ?>
	<!-- PÁGINA DE CONTATO -->
	<div class="pg pg-contato">
		
		<!-- BANNER TOPO -->
		<figure class="bannerTopo" style="background:url(<?php echo $configuracao['contato_banner']['url'] ?>)"></figure>
		<small id="contato"></small>
		<!-- TÍTULO -->
		<div class="areaTitulos">
			<h4 class="tituloInternos">Nos Encontre</h4>
		</div>

		<section class="nosEncontre">
			
			<div class="areaMapa">
				<div class="mapa">
					<div id="mapContato"></div>
				</div>
				<p><?php echo $configuracao['contato_nome']?><strong><?php echo $configuracao['contato_rua'] ?></strong></p>
			</div>


			<div class="areaInformacoesContato">
				<!-- TÍTULO -->
				<div class="titulo">
					<h4>Entre em contato</h4>
				</div>
				<ul>
				<?php if ($configuracao['contato_rh']): ?>
					<li>
						<a href="tel:<?php echo $configuracao['contato_rh']?>">
							<strong>RH:</strong>
							<?php echo $configuracao['contato_rh']?>
						</a>
					</li>
				<?php endif;if ($configuracao['contato_duvidas']): ?>
					<li>
						<a href="tel:<?php echo $configuracao['contato_duvidas']?>">
							<strong>Dúvidas:</strong>
							<?php echo $configuracao['contato_duvidas']?>
						</a>
					</li>
					<?php endif;if ($configuracao['contato_outros']): ?>
					<li>
						<a href="tel:<?php echo $configuracao['contato_outros']?>">
							<strong>Outros assuntos:</strong>
							<?php echo $configuracao['contato_outros']?>
						</a>
					</li>
					<?php endif;if ($configuracao['contato_email']): ?>
					<li>
						<a href="malito:<?php echo $configuracao['contato_email']?>">
							<?php echo $configuracao['contato_email']?>
						</a>
					</li>
				<?php endif; ?>
				</ul>
				<button class="btnReserva">Entrar em contato</button>
			</div>

		</section>

	</div>

	

<?php get_footer(); ?>
