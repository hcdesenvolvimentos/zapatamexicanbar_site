

<?php
/**
 * Template Name: Eventos
 * Description: 
 *
 * @package Zapata_Mexican_Bar
 */
global $configuracao;

get_header(); ?>.

	<!-- PG CARDÁPIO -->
	<div class="pg pg-cardapio pg-eventos">
		<!-- BANNER TOPO -->
		<figure class="bannerTopo" style="background:url(<?php echo $configuracao['eventos_banner']['url'] ?>)"></figure>
		<small id="cardapio"></small>

		<!-- TÍTULO -->
		<div class="areaTitulos">
			<h4 class="tituloInternos">Faça seu Evento<br> no Zapata!</h4>
		</div>
		
		<section class="produto">
			<div class="containerProduto">

				<div class="row areaRow">
				<?php 
					$i = 1;
					//LOOP DE POST DESTAQUES
					$postEventos = new WP_Query( array( 'post_type' => 'eventos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $postEventos->have_posts() ) : $postEventos->the_post();
					$fotoEventos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoEventos = $fotoEventos[0];
					$destaque_mobile = rwmb_meta('baseZapatamexicanBar_eventos_link');
					
				 ?>
					<div class="col-md-4  food-item" data-product-id="<?php echo $id ?>" data-ga="send,event,product,click,detailsOpen">
						<div class="inner">  
							<div class="areaProduto">
								<img src="<?php echo $fotoEventos ?>" />
								<h2><span><?php echo get_the_title() ?></span></h2>

							</div>    
						</div>    
					</div>    

			       <?php $i++;endwhile; wp_reset_query(); ?>
				</div>

			</div>

		</section>

	</div>
<script>	
//FUNÇÃO DE CLIQUE BTNS RESERVA
	$(".btnReserva").click(function(e) {
	 	 $(".fazerReserva").fadeIn("slow");
	 	 setTimeout(function(){
			$(".formReserva").slideDown("slow");
		}, 200);
	});
	$(".closeModal").click(function(e) {
	 	$(".formReserva").fadeOut("slow");
	 	setTimeout(function(){
 			$(".fazerReserva").slideUp("slow");
 		}, 200);
	});
</script>
<?php get_footer();include (TEMPLATEPATH . '/inc/griPluginEventos.php'); ?>
