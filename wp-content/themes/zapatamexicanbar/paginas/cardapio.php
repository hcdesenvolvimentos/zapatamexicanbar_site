

<?php
/**
 * Template Name: Cardápio
 * Description: 
 *
 * @package Zapata_Mexican_Bar
 */
global $configuracao;

get_header(); ?>
	<!-- PG CARDÁPIO -->
	<div class="pg pg-cardapio">
		<!-- BANNER TOPO -->
		<figure class="bannerTopo" style="background:url(<?php echo $configuracao['cardapio_banner']['url'] ?>)"></figure>
		<small  id="cardapio"></small>
		<!-- TÍTULO -->
		<div class="areaTitulos">
			<h4 class="tituloInternos"><?php echo get_the_title() ?></h4>
		</div>
		
		<!-- CARDÁPIO -->
		<div class="containerGeral ">
			<?php 
				// RECUPERANDO CATEGORIAS
				$categoriasCardapio = array(
					'taxonomy'     => 'categoriaCardapio',
					'child_of'     => 0,
					'parent'       => 0,
					'orderby'      => 'name',
					'pad_counts'   => 0,
					'hierarchical' => 1,
					'title_li'     => '',
					'hide_empty'   => 0
				);
				$listaCategorias = get_categories($categoriasCardapio);
				if ($categoriasCardapio ):
					
			?>
			<section class="cardapio">
				
				<ul>
					<?php foreach ($listaCategorias  as $listaCategorias):
						$listaCategoria = $listaCategorias;
						$categoriaAtivaImg = z_taxonomy_image_url($listaCategoria->term_id);
					?>

					<li>
						<a href="<?php echo get_category_link($listaCategoria->cat_ID); ?>/#cardapio">
							<img src="<?php echo $categoriaAtivaImg ?>" alt="<?php echo $listaCategoria->name ?>">
							<h2 class="tituloCardapio">
								<span><?php echo $listaCategoria->name ?></span>
							</h2>
						</a>
					</li>
				<?php endforeach; ?>
				
				</ul>
			</section>
		<?php endif; ?>
		</div>

	</div>
<?php get_footer(); ?>
