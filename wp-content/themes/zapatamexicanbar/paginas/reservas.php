

<?php
/**
 * Template Name: Reservas
 * Description: 
 *
 * @package Zapata_Mexican_Bar
 */
global $configuracao;

get_header(); ?>
	<!-- PG RESERVA -->
	<div class="pg pg-inicial pg-reserva">
		
		<!-- BANNER TOPO -->
		<figure class="bannerTopo" style="background:url(<?php echo $configuracao['reserva_banner']['url'] ?>)"></figure>
		<small id="reservas"></small>
		<!-- TÍTULO -->
		<div class="areaTitulos">
			<h4 class="tituloInternos">Reserve Agora</h4>
		</div>
		
		<!-- SESSÃO RESERVAS -->
		<section class="sessaoReservas sessao">
			<h3 class="hidden">Reservas</h3>
			
			<div class="row">
				
				<!-- FOTO ILUSTRATIVA -->
				<div class="col-sm-4  col-xs-2">
					<figure class="imgIlustrativa left">
						<img class="img-responsive" src="<?php echo $configuracao['areaReservas_imagemLeft']['url'] ?>" alt="Reserva">
					</figure>
				</div>

				<div class="col-sm-4  col-xs-8">
					<address class="areaReservas">
						<h3><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $configuracao['contato_nome'] ?></h3>
						<p><?php echo $configuracao['contato_rua'] ?></p>
						
						<button class="btnReserva">Fazer reserva</button>
						<button class="btnReservaRestorando" >Reservar com Restorando</button>
						<br>	
						<p><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['contato_duvidas'] ?></p>
					</address>
				</div>
				
				<!-- FOTO ILUSTRATIVA -->
				<div class="col-sm-4 col-xs-2">
					<figure class="imgIlustrativa right">
						<img class="img-responsive" src="<?php echo $configuracao['areaReservas_imagemRight']['url'] ?>" alt="Reserva">
					</figure>

				</div>

			</div>
		</section>

	</div>
<?php get_footer(); ?>
