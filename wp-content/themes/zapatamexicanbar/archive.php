<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Zapata_Mexican_Bar
 */

$categoriaAtual = get_the_category();
$categoriaAtual = $categoriaAtual[0]->cat_name;
var_dump($categoriaAtual);
get_header(); ?>

<!-- PG NOTÍCIAS -->
	<div class="pg pg-noticias">
		<!-- BANNER TOPO -->
		<figure class="bannerTopo" style="background:url(<?php echo $configuracao['noticia_banner']['url'] ?>)"></figure>
		<small id="noticias"></small>
		<!-- TÍTULO -->
		<div class="areaTitulos"  >
			<h4 class="tituloInternos">Notícias</h4>
		</div>
		
		<section class="noticias">
			
			<div class="container">
				<div class="row">
					<?php get_sidebar() ?>
					<div class="col-sm-8">
						<div class="areaNoticias">
							
							<ul class="listaPOst">
								<?php 
					            	
									if ( have_posts() ) : while( have_posts() ) : the_post();
									$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$fotoBlog = $fotoBlog[0];
									global $post;
									$categories = get_the_category();
								?>
								<li style="border-color: <?php echo $categories[0]->description ?>;">
									<a href=" <?php echo get_permalink() ?>">
										<figure>
											<img src="<?php echo $fotoBlog ?>" alt="<?php echo get_the_title() ?> ">
										</figure>
										<h2><?php echo get_the_title() ?></h2>
										<span><?php the_time('j \d\e F \d\e Y') ?></span>
										<p><?php customExcerpt(60); ?></p>
									</a>
								</li>
								<?php endwhile;endif; wp_reset_query(); ?>

							</ul>
							
							<div class="paginador">
								<ul>
									<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
								</ul>	
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</section>
	</div>


<?php
get_footer();
