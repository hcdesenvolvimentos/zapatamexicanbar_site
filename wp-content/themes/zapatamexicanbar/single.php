<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Zapata_Mexican_Bar
 */
$imagemDestacada = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(300,300) );
$imagemDestacada = $imagemDestacada[0];
get_header(); ?>
	<!-- PG A MARCA -->
	<div class="pg pg-marca">
		
		<!-- BANNER TOPO -->
		<figure  class="bannerTopo" style="background:url(<?php echo $imagemDestacada  ?>)"></figure>
		<small  id="a-marca"></small>
		<!-- SOBRE NÓS  -->
		<section class="aMarca">
			<div class="container">
				<a href="<?php echo home_url('/noticias/'); ?>" class="voltar">Voltar <</a>
			</div>
			<!-- TÍTULO -->
			<div class="areaTitulos">
				<h4 class="tituloInternos"><?php echo get_the_title() ?></h4>
			</div>
			<span class="data"><?php the_time('j/m/Y') ?></span>

			<!-- TEXTO -->
			<div class="container">
				<article>
					<?php echo the_content() ?>
					
				 <?php if ( comments_open() || get_comments_number() ) : comments_template(); endif; ?>
				</article>
			</div>

		</section>

	</div>
	
<?php

get_footer();
