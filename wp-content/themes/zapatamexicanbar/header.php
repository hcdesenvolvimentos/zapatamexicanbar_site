<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Zapata_Mexican_Bar
 */
global $configuracao;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-67616006-25', 'auto');
		  ga('send', 'pageview');

	</script>
		<!-- TOPO -->
		<header class="topo">
			<div class="containerMenu">
				<div class="row">
					
					<!-- LOGO -->
					<div class="col-sm-2">
						<a href="<?php echo home_url('/'); ?>">
							<img class="img-responsive" src="<?php echo $configuracao['topo_logo']['url'] ?>" alt="Logo Zapata La Taquria">
						</a>
					</div>

					<!-- MENU  -->	
					<div class="col-sm-10">
						<div class="navbar" role="navigation">	
											
							<!-- MENU MOBILE TRIGGER -->
							<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

							<!--  MENU MOBILE-->
							<div class="row navbar-header">			
								<nav class="collapse navbar-collapse" id="collapse">
									<?php 
										$menu = array(
											'theme_location'  => '',
											'menu'            => 'Menu Principal',
											'container'       => false,
											'container_class' => '',
											'container_id'    => '',
											'menu_class'      => 'nav navbar-nav',
											'menu_id'         => '',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
											'depth'           => 2,
											'walker'          => ''
											);
										wp_nav_menu( $menu );
									?>
								</nav>						
							</div>			
							
						</div>
					</div>
				</div>
			</div>

		</header>
