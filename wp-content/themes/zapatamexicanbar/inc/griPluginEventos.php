<script type="text/javascript">
    window.BK_MENU_PRODUCTS = {
	<?php 
		$i = 1;
		//LOOP DE POST DESTAQUES
		$postEventos = new WP_Query( array( 'post_type' => 'eventos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
		while ( $postEventos->have_posts() ) : $postEventos->the_post();
			$fotoEventos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$fotoEventos = $fotoEventos[0];
			$destaque_mobile = rwmb_meta('baseZapatamexicanBar_eventos_link');
			$desc_eventos = rwmb_meta('baseZapatamexicanBar_eventos_brevedescicao');
	 ?>
	       "<?php echo $id ?>": { 
	        "id": <?php echo $id ?>, 
	        "title":"<?php echo get_the_title() ?>", 
	        "subtitle":" ", 
	        "link":"<?php echo $destaque_mobile ?> ", 
	        "image":"<?php echo $fotoEventos  ?>",
	        "description":"<?php echo $desc_eventos ?>", 
	      },
	     <?php $i++;endwhile; wp_reset_query(); ?>
     
	}
</script>

 
