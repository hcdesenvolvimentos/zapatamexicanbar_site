<script type="text/javascript">
    window.BK_MENU_PRODUCTS = {
		<?php 
			$id = 0; 
			if ( have_posts() ) : while( have_posts() ) : the_post();  
			$fotoPrato = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$fotoPrato = $fotoPrato[0];
		?>
	       "<?php echo $id ?>": { 
	        "id": <?php echo $id ?>, 
	        "title":"<?php echo get_the_title() ?>", 
	        "subtitle":" ", 
	        "image":"<?php echo $fotoPrato  ?>",
	        "description":"<?php echo get_the_content() ?>", 
	      },
	    <?php $id++; endwhile;endif; wp_reset_query(); ?> 
     
	}
</script>

 