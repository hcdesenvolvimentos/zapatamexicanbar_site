<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Zapata_Mexican_Bar
 */

get_header(); ?>

	<!-- PG NOTÍCIAS -->
	<div class="pg pg-noticias">
		<!-- BANNER TOPO -->
		<figure class="bannerTopo" style="background:url(<?php echo $configuracao['noticia_banner']['url'] ?>)"></figure>
		<small id="noticias"></small>
		<!-- TÍTULO -->
		<div class="areaTitulos">
			<h4 class="tituloInternos">Notícias</h4>
		</div>
		
		<section class="noticias" >
			
			<div class="container">
				<div class="row">
					<?php get_sidebar() ?>
					<div class="col-sm-8">
						<div class="areaNoticias">
							
							<ul class="listaPOst">
								<?php 
									
									
					            	
									if ( have_posts() ) :while ( have_posts() ) : the_post();
									$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$fotoBlog = $fotoBlog[0];
									global $post;
									$categories = get_the_category();
								?>
								<li style="border-color: <?php echo $categories[0]->description ?>;">
									<a href=" <?php echo get_permalink() ?>">
										<figure>
											<img src="<?php echo $fotoBlog ?>" alt="<?php echo get_the_title() ?> ">
										</figure>
										<h2><?php echo get_the_title() ?></h2>
										<span><?php the_time('j/F/Y') ?></span>
										<p><?php customExcerpt(60); ?></p>
									</a>
								</li>
								<?php endwhile;endif; wp_reset_query(); ?>

							</ul>
							<div class="paginador">
								<ul>
								<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
								</ul>	
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</section>
	</div>


<?php
 
get_footer();
