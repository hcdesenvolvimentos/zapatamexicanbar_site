<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Zapata_Mexican_Bar
 */
$categoriaAtual = get_term_by( 'name', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

get_header(); ?>
<!-- PÁGINA DE PROMOÇÕES -->
<div class="pg pg-promocoes">
	<!-- BANNER TOPO -->
	<figure class="bannerTopo" style="background:url(<?php echo $configuracao['banner_promocao']['url'] ?>)"></figure>

	<!-- TÍTULO -->
	<div class="areaTitulos">
		<h4 class="tituloInternos">Promoções</h4>
	</div>

	<nav>
		<ul id="cardapio">
			<?php 
				// RECUPERANDO CATEGORIAS
				$categoriasCardapio = array(
					'taxonomy'     => 'categoriapromocao',
					'child_of'     => 0,
					'parent'       => 0,
					'orderby'      => 'name',
					'pad_counts'   => 0,
					'hierarchical' => 1,
					'title_li'     => '',
					'hide_empty'   => 0
				);
				$listaCategorias = get_categories($categoriasCardapio);
					
				 foreach ($listaCategorias  as $listaCategorias):
					$listaCategoria = $listaCategorias;

				if ($listaCategoria->name  == $categoriaAtual->name ):
			?>
			<li class="ative">
				<a href="<?php echo get_category_link($listaCategoria->cat_ID); ?>/#cardapio">
					<h2><?php echo $listaCategoria->name ?></h2>
					<img src=" <?php echo $categoriaAtivaImg = z_taxonomy_image_url($listaCategoria->term_id) ?>" alt="<?php echo $listaCategoria->name ?>">
				</a>
			</li>
			<?php else: ?>
			<li>
				<a href="<?php echo get_category_link($listaCategoria->cat_ID); ?>/#cardapio">
					<h2><?php echo $listaCategoria->name ?></h2>
					<img src=" <?php echo $categoriaAtivaImg = z_taxonomy_image_url($listaCategoria->term_id) ?>" alt="<?php echo $listaCategoria->name ?>">
				</a>
			</li>
			<?php endif;endforeach; ?>
		</ul>
	</nav>

	<!-- CARROSSEL DE PORMOÇÕES -->
	<div class="container">
		<section class="carrosselPromocao sessao">
			
			<!-- TÍTULO -->
			<p class="tiutoModeloRight"><?php echo  $categoriaAtual->name ?></p>

			<div id="carrosselPromocaoCardapio" class="owl-Carousel">
				
				<!-- ITEM -->
				<?php 
					if ( have_posts() ) : while( have_posts() ) : the_post();
						$fotoPromocao = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPromocao = $fotoPromocao[0];
						$promocao_mobile = rwmb_meta('baseZapatamexicanBar_promocao_mobile');

						$promocao_link = rwmb_meta('baseZapatamexicanBar_promocao_link');
						foreach ($promocao_mobile  as $promocao_mobile):
							$promocao_mobile = $promocao_mobile['full_url']; 

				 ?>
				<figure class="item" style="background: url(<?php echo $promocao_mobile ?>)">
					<h2 class="hidden"><?php echo get_the_title() ?></h2>
					<a <?php if($promocao_link){ echo 'href="'.$promocao_link.'"';} ?>>
						<img class="img-responsive" src="<?php echo $fotoPromocao  ?>" alt=" <?php echo get_the_title() ?>">
						<?php if ( $promocao_link):	?>
						<strong class="linkTiluto">Saiba mais</strong>
						<?php endif; ?>
					</a>
				</figure>
				 <?php  endforeach;endwhile;endif; wp_reset_query(); ?> 

			</div>

		</section>
	</div>
</div>

 <?php get_footer(); ?>

