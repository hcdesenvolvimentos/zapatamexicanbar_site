<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Zapata_Mexican_Bar
 */
$categoriaAtual = get_term_by( 'name', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
get_header(); ?>

	<!-- PG CARDÁPIO -->
	<div class="pg pg-cardapio">
		<!-- BANNER TOPO -->
		<figure class="bannerTopo" style="background:url(<?php echo $configuracao['cardapio_banner']['url'] ?>)"></figure>
		<small id="cardapio"></small>

		<!-- TÍTULO -->
		<div class="areaTitulos">
			<h4 class="tituloInternos"><?php echo $categoriaAtual->name ?></h4>
		</div>

		<!-- MENU -->
		<section class="cardapio cardapioChili">
			
			<nav>
			<?php 
				// RECUPERANDO CATEGORIAS
				$categoriasCardapio = array(
					'taxonomy'     => 'categoriaCardapio',
					'child_of'     => 0,
					'parent'       => 0,
					'orderby'      => 'name',
					'pad_counts'   => 0,
					'hierarchical' => 1,
					'title_li'     => '',
					'hide_empty'   => 0
				);
				$listaCategorias = get_categories($categoriasCardapio);
					
				 foreach ($listaCategorias  as $listaCategorias):
					$listaCategoria = $listaCategorias;
				if ($listaCategoria->name  == $categoriaAtual->name ):
			?>
				<a class="ativo" href="<?php echo get_category_link($listaCategoria->cat_ID); ?>/#cardapio"><?php echo $listaCategoria->name ?></a>
			<?php else: ?>
				<a href="<?php echo get_category_link($listaCategoria->cat_ID); ?>/#cardapio"><?php echo $listaCategoria->name ?></a>
			<?php endif;endforeach; ?>
			</nav>

		</section>
		

		<section class="produto">
			<div class="containerProduto">

				<div class="row areaRow">
				<?php 
					$id = 0;
					if ( have_posts() ) : while( have_posts() ) : the_post();
						$fotoPrato = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPrato = $fotoPrato[0];
				 ?>
					<div class="col-md-4  food-item" data-product-id="<?php echo $id ?>" data-ga="send,event,product,click,detailsOpen">
						<div class="inner">  
							<div class="areaProduto">
								<img src="<?php echo $fotoPrato ?>" />
								<h2><span><?php echo get_the_title() ?></span></h2>

							</div>    
						</div>    
					</div>    

			        <?php $id++; endwhile;endif; wp_reset_query(); ?> 
				</div>

			</div>

		</section>

	</div>

<?php
 
 include (TEMPLATEPATH . '/inc/griPluginCardapio.php');

