<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Zapata_Mexican_Bar
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="col-sm-4">
	<nav class="sidebar">
		<p>Categorias</p>
		<?php

			// LISTA DE CATEGORIAS
			$arrayCategorias = array();
			$categorias=get_categories($args);
			foreach($categorias as $categoria):
				$nomeCategoria = $categoria->name;
				$descricao = $categoria->description;

			if ($categoriaAtual == $nomeCategoria ):
		?>
		<a class="ativo" href="<?php echo get_category_link($categoria->cat_ID); ?>/#noticias" style="background:<?php echo $descricao ?> "><?php echo $nomeCategoria; ?></a>
		<?php else: ?>
		<a href="<?php echo get_category_link($categoria->cat_ID); ?>/#noticias" style="background:<?php echo $descricao ?> "><?php echo $nomeCategoria; ?></a>
		<?php endif;endforeach; ?>
		<p class="maisVistos">Mais vistos</p>
		<div class="areaMaisvistos">
		
		<?php dynamic_sidebar( 'sidebar-1' ); ?>

		</div>
		<p class="maisVistos hidden">outros</p>

	</nav>
</div>

