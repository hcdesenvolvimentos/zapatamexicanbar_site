<?php

/**
 * Plugin Name: Base Zapata Mexican Bar
 * Description: Controle base do tema Base Zapata Mexican Bar.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */

	function basebaseZapatamexicanBar () {

		// TIPOS DE CONTEÚDO
		conteudosbaseZapatamexicanBar();

		// TAXONOMIA
		 taxonomiabaseZapatamexicanBar();

		// META BOXES
		metaboxesbaseZapatamexicanBar();

		// SHORTCODES
		shortcodesbaseZapatamexicanBar();

	    // ATALHOS VISUAL COMPOSER
	    //visualcomposerbaseZapatamexicanBar();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosbaseZapatamexicanBar (){

		// TIPOS DE DESTAQUE		
		tipoDestaques();

		// TIPOS DE CARDÁPIO		
		tipoCardapio();

		// TIPOS DE CARDÁPIO		
		tipoPromocoes();

		// TIPOS DE CARDÁPIO		
		tipoEventos();
	
		// ALTERAÇÃO DO TÍTULO PADRÃO
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				
				default:
				break;
			}

	     	return $titulo;

		}

	}

		//CONTEÚDO: DESTAQUES
		function tipoDestaques() {

				$rotulosDestaques = array(
										'name'               => 'destaque',
										'singular_name'      => 'destaques',
										'menu_name'          => 'Destaque',
										'name_admin_bar'     => 'destaque',
										'add_new'            => 'Adicionar novo destaque',
										'add_new_item'       => 'Adicionar novo destaque',
										'new_item'           => 'Novo destaque',
										'edit_item'          => 'Editar destaque',
										'view_item'          => 'Ver destaque',
										'all_items'          => 'Todos destaques',
										'search_items'       => 'Buscar destaque',
										'parent_item_colon'  => 'Dos destaques',
										'not_found'          => 'Nenhum destaque cadastrada.',
										'not_found_in_trash' => 'Nenhum destaque na lixeira.'
									);

				$argsDestaques 	= array(
										'labels'             => $rotulosDestaques,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 4,
										'menu_icon'          => 'dashicons-format-image',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'destaques' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title','thumbnail' )
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('destaques', $argsDestaques);

		}

		//CONTEÚDO: CARDÁPIO
		function tipoCardapio() {

				$rotulosCardapio = array(
										'name'               => 'prato',
										'singular_name'      => 'platos',
										'menu_name'          => 'Menú',
										'name_admin_bar'     => 'prato',
										'add_new'            => 'Adicionar novo prato',
										'add_new_item'       => 'Adicionar novo prato',
										'new_item'           => 'Novo prato',
										'edit_item'          => 'Editar prato',
										'view_item'          => 'Ver prato',
										'all_items'          => 'Todos os pratos',
										'search_items'       => 'Buscar prato',
										'parent_item_colon'  => 'Dos pratos',
										'not_found'          => 'Nenhum prato cadastrada.',
										'not_found_in_trash' => 'Nenhum prato na lixeira.'
									);

				$argsCardapio 	= array(
										'labels'             => $rotulosCardapio,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 4,
										'menu_icon'          => 'dashicons-editor-ul',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'cardapio-zapata-mexican-bar' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title','thumbnail','editor')
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('cardapio', $argsCardapio);

		}

		//CONTEÚDO: PROMOÇÕES
		function tipoPromocoes() {

				$rotulosPromocoes = array(
										'name'               => 'promoção',
										'singular_name'      => 'Promoções',
										'menu_name'          => 'Promoções',
										'name_admin_bar'     => 'promoção',
										'add_new'            => 'Adicionar nova promoção',
										'add_new_item'       => 'Adicionar nova promoção',
										'new_item'           => 'Nova promoção',
										'edit_item'          => 'Editar promoção',
										'view_item'          => 'Ver promoção',
										'all_items'          => 'Todas as promoções ',
										'search_items'       => 'Buscar promoção',
										'parent_item_colon'  => 'Das promoções',
										'not_found'          => 'Nenhuma promoção cadastrada.',
										'not_found_in_trash' => 'Nenhuma promoção na lixeira.'
									);

				$argsPromocoes 	= array(
										'labels'             => $rotulosPromocoes,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 4,
										'menu_icon'          => 'dashicons-megaphone',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'promocoes' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title','thumbnail','editor')
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('promocoes', $argsPromocoes);

		}

		//CONTEÚDO: EVENTOS
		function tipoEventos() {

				$rotulosEventos = array(
										'name'               => 'eventos',
										'singular_name'      => 'evento',
										'menu_name'          => 'Eventos',
										'name_admin_bar'     => 'evento',
										'add_new'            => 'Adicionar novo evento',
										'add_new_item'       => 'Adicionar novo evento',
										'new_item'           => 'Novo evento',
										'edit_item'          => 'Editar evento',
										'view_item'          => 'Ver evento',
										'all_items'          => 'Todos os eventos ',
										'search_items'       => 'Buscar evento',
										'parent_item_colon'  => 'Dos eventos',
										'not_found'          => 'Nenhum evento cadastrada.',
										'not_found_in_trash' => 'Nenhum evento na lixeira.'
									);

				$argsEventos 	= array(
										'labels'             => $rotulosEventos,
										'public'             => true,
										'publicly_queryable' => true,
										'show_ui'            => true,
										'show_in_menu'       => true,
										'menu_position'		 => 4,
										'menu_icon'          => 'dashicons-calendar-alt',
										'query_var'          => true,
										'rewrite'            => array( 'slug' => 'eventos' ),
										'capability_type'    => 'post',
										'has_archive'        => true,
										'hierarchical'       => false,
										'supports'           => array( 'title','thumbnail','editor')
									);

				// REGISTRA O TIPO CUSTOMIZADO
				register_post_type('eventos', $argsEventos);

		}
		
	/****************************************************
	* TAXONOMIA
	*****************************************************/
	 function taxonomiabaseZapatamexicanBar () {
	 
	 	taxCategoriaDestaque();

	 	taxCategoriaCardapio();

	 	taxCategoriaPromocao();
	 		 	
	 }
	 
		//TAXONOMIA: DESTAQUE
		function taxCategoriaDestaque() {

		 		$rotulos 			= array(
		 								'name'              => 'Categorias de destaque',
										'singular_name'     => 'Categoria de destaque',
										'search_items'      => 'Buscar categorias de destaque',
										'all_items'         => 'Todas categorias de destaque',
										'parent_item'       => 'Categoria pai',
										'parent_item_colon' => '',
										'edit_item'         => 'Editar categoria de destaque',
										'update_item'       => 'Atualizar categoria de destaque',
										'add_new_item'      => 'Adicionar nova categoria de destaque',
										'new_item_name'     => 'Nova categoria de destaque',
										'menu_name'         => 'Categorias de destaque',
		 		);

		 		$args 				= array(
		 								'hierarchical'      => true,
		 								'labels'            => $rotulos,
		 								'show_ui'           => true,
		 								'show_admin_column' => true,
		 								'query_var'         => true,
		 								'rewrite'           => array( 'slug' => 'categoria-destaque' ),
		 		);

		 	register_taxonomy( 'categoriaDestaque', array( 'destaques' ), $args );
		}

		//TAXONOMIA: CARDÁPIO
		function taxCategoriaCardapio() {

		 		$rotulos 			= array(
		 								'name'              => 'Categorias de cardápio',
										'singular_name'     => 'Categoria de cardápio',
										'search_items'      => 'Buscar categorias de cardápio',
										'all_items'         => 'Todas categorias de cardápio',
										'parent_item'       => 'Categoria pai',
										'parent_item_colon' => '',
										'edit_item'         => 'Editar categoria de cardápio',
										'update_item'       => 'Atualizar categoria de cardápio',
										'add_new_item'      => 'Adicionar nova categoria de cardápio',
										'new_item_name'     => 'Nova categoria de cardápio',
										'menu_name'         => 'Categorias de cardápio',
		 		);

		 		$args 				= array(
		 								'hierarchical'      => true,
		 								'labels'            => $rotulos,
		 								'show_ui'           => true,
		 								'show_admin_column' => true,
		 								'query_var'         => true,
		 								'rewrite'           => array( 'slug' => 'categoria-cardapio' ),
		 		);

		 	register_taxonomy( 'categoriaCardapio', array( 'cardapio' ), $args );
		}

		//TAXONOMIA: PROMOÇÕES
		function taxCategoriaPromocao() {

		 		$rotulos 			= array(
		 								'name'              => 'Categorias de promoção',
										'singular_name'     => 'Categoria de promoção',
										'search_items'      => 'Buscar categorias de promoção',
										'all_items'         => 'Todas categorias de promoção',
										'parent_item'       => 'Categoria pai',
										'parent_item_colon' => '',
										'edit_item'         => 'Editar categoria de promoção',
										'update_item'       => 'Atualizar categoria de promoção',
										'add_new_item'      => 'Adicionar nova categoria de promoção',
										'new_item_name'     => 'Nova categoria de promoção',
										'menu_name'         => 'Categorias de promoção',
		 		);

		 		$args 				= array(
		 								'hierarchical'      => true,
		 								'labels'            => $rotulos,
		 								'show_ui'           => true,
		 								'show_admin_column' => true,
		 								'query_var'         => true,
		 								'rewrite'           => array( 'slug' => 'categoria-promocao' ),
		 		);

		 	register_taxonomy( 'categoriapromocao', array( 'promocoes' ), $args );
		}
		
    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesbaseZapatamexicanBar(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'baseZapatamexicanBar_';

			
			wp_reset_query();

			// DESTAQUES
			$metaboxes[] = array(

				'id'			=> 'MetaboxDestaque',
				'title'			=> 'Detalhes do destaque',
				'pages' 		=> array( 'destaques' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
			 			'name'  => 'Imagem 320 X 320',
			 			'id'    => "{$prefix}destaque_mobile",
			 			'desc'  => '',
			 			'desc'  => 'imagem mobilie',
			 			'type'  => 'image_advanced',
			 			'max_file_uploads'  => 1,
			 		),
			 		array(
			 			'name'  => 'Link banner',
			 			'id'    => "{$prefix}destaque_link",
			 			'desc'  => '',
			 			'type'  => 'text',

			 		),
			 		
				),
			);

			
			// DESTAQUES
			$metaboxes[] = array(

				'id'			=> 'MetaboxPromocao',
				'title'			=> 'Detalhes da promoção',
				'pages' 		=> array( 'promocoes' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
			 			'name'  => 'Imagem 320 X 320',
			 			'id'    => "{$prefix}promocao_mobile",
			 			'desc'  => '',
			 			'desc'  => 'imagem mobilie',
			 			'type'  => 'image_advanced',
			 			'max_file_uploads'  => 1,
			 		),
			 		array(
			 			'name'  => 'Link banner',
			 			'id'    => "{$prefix}promocao_link",
			 			'desc'  => '',
			 			'type'  => 'text',

			 		),
			 		
				),
			);

			// EVENTOS
			$metaboxes[] = array(

				'id'			=> 'MetaboxeVENTOS',
				'title'			=> 'Detalhes do eventos',
				'pages' 		=> array( 'eventos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
			 		array(
			 			'name'  => 'Link banner',
			 			'id'    => "{$prefix}eventos_link",
			 			'desc'  => '',
			 			'type'  => 'text',

			 		),
			 		array(
			 			'name'  => 'Breve descrição',
			 			'id'    => "{$prefix}eventos_brevedescicao",
			 			'desc'  => '',
			 			'type'  => 'textarea',

			 		),
			 		
				),
			);

			return $metaboxes;
		}
		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesbaseZapatamexicanBar(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerbaseZapatamexicanBar(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'basebaseZapatamexicanBar');

	add_action( 'add_meta_boxes', 'metaboxjs');
	// FLUSHS
	function rewrite_flush() {

    	basebaseZapatamexicanBar();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );